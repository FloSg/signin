<?php 

session_start(); 

/* Variables génériques pour le layout */
const LAYOUT_VIEW = 'user/logIn';
const LAYOUT_TITLE = 'Se connecter';

require('src/models/user.php');

$errors = [];

// Fonction de recherche user dans DB

if(isset($_POST['email'])) {

    /* Le formulaire est posté - On récupère les données de formulaire*/
    $dataForm = [
        'email'  => trim($_POST['email']),
        'password' => trim($_POST['password']),
    ];


    /* Validation des données transmises */
    
    if(!filter_var($dataForm['email'], FILTER_VALIDATE_EMAIL))
        $errors['email'] = 'L\'email n\'est pas valide';

    if(empty($dataForm['password']))
        $errors['password'] = 'Le password ne peut-être vide';

    /* Si pas d'erreurs dans le logIn */
    if(empty($errors)) {

        try {

        
            /* Connexion au SGBD Mysql */
            $dbh = dbConnect();
            $user = getUser($dbh, $dataForm['email']);

            //var_dump($user);

        /* Si l'utilisateur existe */
            if (!empty($user)) {
    
                /* Si le mot de passe reçu est le même que le mot de passe dans la base */
                $hash = $user['password'];
                if (password_verify($dataForm['password'], $hash)) {
                    /* On écris dans la session un index connected à vrai */
                    $_SESSION['connected'] = true;
                    /* On écris dans la session un index user avec les données utilisateur (jeu d'enregistrement obtenu avec la requête) */
                    $_SESSION['user'] = $user;
                    /* On redirige l'utilisateur vers la page d'accueil du backOffice index.php */
                    header("Location: index.php");
                    exit();
                }
                else {
                    $errors['connexion'] = 'Erreur de connexion';
                }
        }
    else {
        $errors['connexion'] = 'Erreur de connexion';
    }

    }

        catch (PDOException $e) {
            echo 'Navré notre serveur de BDD est down !';
            echo $e->getMessage();
        }

        catch (DomainException $e) {
            /** Inclu le layout d'erreur */
            $message = $e->getMessage();
            require('tpl/error.phtml');
            exit();
        }
    }
    else {
        $errors['connexion'] = 'Erreur de connexion';
    }
}






/*
if(isset($_SESSION['connected']) && $_SESSION['connected'] == true) {
    header('Location:index.php');
    exit();
}
*/
//** Si l'utilisateur est trouvé dans la base de données */
/*
if(true)
{
    echo '<br>login automatique OK !';
    $_SESSION['connected']  = true;
    $_SESSION['user']       = 'toto';
}

*/

require('views/admin/layout.phtml');
