<?php

session_start(); 

require('lib/secure.php');


isAuthentificated(); 

var_dump($_POST);
var_dump($_FILES);
var_dump($_SESSION);
/* On inclu le modèle */
require('src/models/user.php');

/* Variables génériques pour le layout */
const LAYOUT_VIEW = 'user/signIn';
const LAYOUT_TITLE = 'Ajouter un utilisateur';

//Création d'une variable date pour la DB
$date = date("Y-m-d");

/** Creation d'un tableau d'erreur vide */
$errors = [];



if(isset($_POST['firstname'])) {

    /* Le formulaire est posté - On récupère les données de formulaire*/
    $dataForm = [
        'firstname'  => trim($_POST['firstname']),
        'lastname'   => trim($_POST['lastname']),
        'email'      => trim($_POST['email']),
        'password'      => trim($_POST['password']),
        'password2'   => trim($_POST['password2']),
        'bio'   => trim($_POST['bio']),
        'role'   => trim($_POST['role']),
        'avatar' => $_FILES['fileToUpload']['name']
    ];

    /* Validation des données transmises */
    if(empty($dataForm['firstname']) || strlen($dataForm['firstname']) < 2)
        $errors['firstname']    = 'Le prénom ne peut-être vide ou inférieur à 2 caractères';

    if(empty($dataForm['lastname']) || strlen($dataForm['lastname']) < 2)
        $errors['lastname']    = 'Le nom ne peut-être vide ou inférieur à 2 caractères';
    
    if(!filter_var($dataForm['email'], FILTER_VALIDATE_EMAIL))
        $errors['email']    = 'L\'email n\'est pas valide';

    if(empty($dataForm['password']) || strlen($dataForm['password']) < 8)
        $errors['password']    = 'Le password ne peut-être vide ou inférieur à 8 chiffres';

    if(empty($dataForm['password2']) && ($dataForm['password2'] !== $dataForm['password']) )
        $errors['password2'] = 'Les passwords doivent être identiques';

    // Verifier avatar

    if(isset($_FILES['fileToUpload']) AND !empty($_FILES['fileToUpload']['name'])) {
        //Definition de la taille max
        $tailleMax = /*2097152*/ 9999999999;
        //Definition des extensions valides
        $extensionsValides = array('jpg', 'jpeg', 'gif', 'png');
        //Si la taille de l'upload est conforme
        if($_FILES['fileToUpload']['size'] <= $tailleMax) {
            //Verificatin de l'extension de l'upload
           $extensionUpload = strtolower(substr(strrchr($_FILES['fileToUpload']['name'], '.'), 1));
           //Si l'extension est valide en accord avec les extensions valides
           if(in_array($extensionUpload, $extensionsValides)) {
               //On bouge le fichier dans le chemin "uploads/"
              $chemin = "assets/img/avatar/".$dataForm['lastname'].$dataForm['firstname'].".".$extensionUpload;
              $resultat = move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $chemin);
              if($resultat) {
                 $avatar = $dataForm['lastname'].$dataForm['firstname'].".".$extensionUpload;
              } else {
                 $errors['avatar'] = "Erreur durant l'importation de votre photo de profil";
              }
           } else {
              $errors['avatar'] = "Votre photo de profil doit être au format jpg, jpeg, gif ou png";
           }
        } else {
           $errors['avatar'] = "Votre photo de profil ne doit pas dépasser 2Mo";
        }
        
     }
     else {
        $avatar = null;
    }
    
    // FIN DE VERIF

        /* Si pas d'erreurs dans le formulaire d'inscription */
    if(empty($errors)) {

        // On enregistre les données dans la BDD après avoir hashé le mdp 
        try {
            /* Connexion au SGBD Mysql */
            $dbh = dbConnect();
            /* Hashage du password */
            $password = password_hash($dataForm['password'], PASSWORD_DEFAULT);

            $user = insertNewUser($dbh, $dataForm['lastname'], $dataForm['firstname'], $dataForm['email'], $password, $dataForm['bio'], $dataForm['role'], $avatar, $date); 
            header("Location: index.php?controller=userlist");
            exit();
        }

        catch (PDOException $e) {
            echo 'Navré notre serveur de BDD est down !';
            echo $e->getMessage();
        }

        catch (DomainException $e) {
            /** Inclu le layout d'erreur */
            $message = $e->getMessage();
            require('tpl/error.phtml');
            exit();
        }
    }
}

/** Inclu le layout */
require('views/admin/layout.phtml');