<?php

session_start(); 

if(!isset($_SESSION['connected']) || $_SESSION['connected'] !== true) {
    header('Location:index.php?controller=logIn');
    exit();
}

/* Variables génériques pour le layout */
const LAYOUT_VIEW = 'home';
const LAYOUT_TITLE = 'Dashboard';

/** Inclu le layout */
require('tpl/layout.phtml');