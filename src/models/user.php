<?php



function insertNewUser(PDO $dbh, string $lastname, string $firstname, string $email, string $password, string $bio, string $role, $avatar, string $createdAt) {

    $sth = $dbh->prepare('INSERT INTO user (lastname, firstname, email, password, bio, role, avatar, createdAt)
    VALUES
    (:lastname, :firstname, :email, :password, :bio, :role, :avatar, :createdAt)');

    $sth->bindValue(':lastname', $lastname, PDO::PARAM_STR);
    $sth->bindValue(':firstname', $firstname, PDO::PARAM_STR);
    $sth->bindValue(':email', $email, PDO::PARAM_STR);
    $sth->bindValue(':password', $password, PDO::PARAM_STR);
    $sth->bindValue(':bio', $bio, PDO::PARAM_STR);
    $sth->bindValue(':role', $role, PDO::PARAM_STR);
    $sth->bindValue(':avatar', $avatar);
    $sth->bindValue(':createdAt', $createdAt, PDO::PARAM_STR);

    $sth->execute();
}

function getUser( PDO $dbh, string $email) {
    $sth = $dbh->prepare('SELECT * FROM user WHERE email = :email');

    $sth->bindValue(':email', $email, PDO::PARAM_STR);
    $sth->execute();
    $users = $sth->fetch(PDO::FETCH_ASSOC);

    return $users;
}

