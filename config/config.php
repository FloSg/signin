<?php

// On dit à PHP d'afficher les erreurs pour ce script
ini_set('display_errors', '1');
// On dit à PHP d'afficher les erreur de démarrage pour ce script
ini_set('display_startup_errors', '1');
// On dit à PHP de nous reporter toutes les erreurs 
error_reporting(E_ALL);

function exception_error_handler($severity, $message, $file, $line) {
    if (!(error_reporting() & $severity)) {
        // This error code is not included in error_reporting
        return;
    }
    throw new ErrorException($message, 0, $severity, $file, $line);
}
set_error_handler("exception_error_handler");


/** Paramètres de connexion Mysql */
const DB_DSN = 'mysql:host=localhost;dbname=blog;charset=utf8';
const DB_USER = 'root';
const DB_PASSWORD = '';
const DEFAULT_CONTROLLER = "home";
const PATH_SRC = "src/";