<?php 

//declare(strict_types=1);

/* On inclu la configuration */
//require('config/config.php');

/* On inclu la libraire */
//require('lib/bdd.php');

/*

$controller = 'home';

if(isset($_GET['controller']))
    $controller = $_GET['controller'];

if(file_exists('src/controllers/'.$controller.'.php'))
    require('src/controllers/'.$controller.'.php');

*/
/* On démarre la session : on dit à PHP d'ouvrir et de déserialiser le fichier de session sur disque
et de mettre le tableau obtenu dans le tableau $_SESSION : */
/*
session_start();
*/
/** L'utilisateur sera redirigé s'il n'est pas connecté */
/*
if(!isset($_SESSION['connected']) || $_SESSION['connected'] !== true) {
    header('Location:login.php');
    exit();
}
*/

declare(strict_types=1);

/* On inclu la configuration */
require('config/config.php');

/* On inclu la libraire */
require('lib/bdd.php');

/** La constante DEFAULT_CONTROLLER doit-être défini dans le fichier config.php
 * const DEFAULT_CONTROLLER = 'home';
 * C'est le controller par défaut si on en fourni pas. Par exemple *home* qui affichera le Dashbord du backOffice 
 */
$controller = DEFAULT_CONTROLLER;

/** Si le controller est passé dans l'URI on le récupère **/
if(isset($_GET['controller']))
    $controller = $_GET['controller'];

/** Si le fichier n'existe pas dans le dossier des controller on lève une exeption
 * cont PATH_SRC = 'src/'; //a définir dans config.php
 */

try
{
    if(!file_exists(PATH_SRC.'controllers/'.$controller.'.php'))
        throw new DomainException('Le controller demandé n\'existe pas');
    
    /* On inclu le controller */
    require('src/controllers/'.$controller.'.php');
}
catch (DomainException $e) {
    /** On répond le contenu du fichier error.phtml (template complet) qui affichera le contenu de la variable $message. On précise aussi le code de réponse 404 (ressource non trouvée !)
    * cont PATH_VIEW = 'views/'; //a définir dans config.php
    */
    $message = $e->get_message();
    require(PATH_VIEW.'error.phtml');
    exit();
}
